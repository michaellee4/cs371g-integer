// ---------------
// TestInteger.c++
// ---------------

// --------
// includes
// --------

#include "gtest/gtest.h"

#include "Integer.hpp"
#include <climits>

using namespace std;

// ----
// plus
// ----

TEST(IntegerFixture, addTest1) {
    Integer<int> x = 0;
    Integer<int> y = 0;
    Integer<int> z = x + y;
    ASSERT_EQ(z, 0);
}

TEST(IntegerFixture, addTest2) {
    Integer<int> x {"9070264019274633792701958"};
    Integer<int> y ("307123009672380923962058");
    Integer<int> z = x + y;
    ASSERT_EQ(z, Integer<int>("9377387028947014716664016"));
}

TEST(IntegerFixture, addTest3) {
    Integer<int> x("9999999");
    Integer<int> y = 1;
    Integer<int> z = x + y;
    ASSERT_EQ(z, Integer<int>("10000000"));
}

TEST(IntegerFixture, addTest4) {
    Integer<int> x("9999999");
    Integer<int> y = -11;
    Integer<int> z = x + y;
    ASSERT_EQ(z, Integer<int>("9999988"));
}

TEST(IntegerFixture, subTest1) {
    Integer<int> x = 0;
    Integer<int> y = 0;
    Integer<int> z = x - y;
    ASSERT_EQ(z, 0);
}

TEST(IntegerFixture, subTest2) {
    Integer<int> x {"9070264019274633792701958"};
    Integer<int> y ("307123009672380923962058");
    Integer<int> z = x - y;
    ASSERT_EQ(z, Integer<int>("8763141009602252868739900"));
}

TEST(IntegerFixture, subTest3) {
    Integer<int> x("-9999999");
    Integer<int> y = 1;
    Integer<int> z = x - y;
    ASSERT_EQ(z, Integer<int>("-10000000"));
}

TEST(IntegerFixture, subTest4) {
    Integer<int> x("-9999999");
    Integer<int> y = -11;
    Integer<int> z = x - y;
    ASSERT_EQ(z, Integer<int>("-9999988"));
}

TEST(IntegerFixture, mulTest1) {
    Integer<int> x = 0;
    Integer<int> y("2785686784122732029647395703030870821286494309564");
    Integer<int> z = x * y;
    ASSERT_EQ(z, 0);
}

TEST(IntegerFixture, mulTest2) {
    Integer<int> x {"9070264019274633792701958"};
    Integer<int> y ("307123009672380923962058");
    Integer<int> z = x * y;
    ASSERT_EQ(z, Integer<int>("2785686784122732029647395703030870821286494309564"));
}

TEST(IntegerFixture, mulTest3) {
    Integer<int> x("-9999999");
    Integer<int> y = -1;
    Integer<int> z = x * y;
    ASSERT_EQ(z, Integer<int>("9999999"));
}

TEST(IntegerFixture, mulTest4) {
    Integer<int> x("9999999");
    Integer<int> y = -11;
    Integer<int> z = x * y;
    ASSERT_EQ(z, Integer<int>("-109999989"));
}

TEST(IntegerFixture, powTest1) {
    Integer<int> x = 0;
    Integer<int> z = x.pow(INT_MAX);
    ASSERT_EQ(x, 0);
}

TEST(IntegerFixture, powTest2) {
    Integer<int> x = 1;
    Integer<int> z = x.pow(INT_MAX);
    ASSERT_EQ(x, 1);
}

TEST(IntegerFixture, powTest3) {
    Integer<int> x("-10");
    Integer<int> z = x.pow(2);
    ASSERT_EQ(z, Integer<int>("100"));
}

TEST(IntegerFixture, powTest4) {
    Integer<int> x("-10");
    Integer<int> z = x.pow(3);
    ASSERT_EQ(z, Integer<int>("-1000"));
}

TEST(IntegerFixture, powTest5) {
    Integer<int> x("-1111111");
    Integer<int> z = x.pow(9);
    ASSERT_EQ(z, Integer<int>("-2581172468656813862820765818092802882193677681296819591"));
}

TEST(IntegerFixture, relationalTest1) {
    Integer<int> x(std::string("-0"));
    Integer<int> y(0);
    ASSERT_EQ(x, y);
}

TEST(IntegerFixture, relationalTest2) {
    Integer<int> x(std::string("12345678987654321"));
    Integer<int> y("12345678987654321");
    ++--y;
    ASSERT_EQ(x, y);
}

TEST(IntegerFixture, relationalTest3) {
    Integer<int> x(std::string("12345678987654321"));
    Integer<int> y("123456789876");
    ASSERT_LT(y, x);
}

TEST(IntegerFixture, relationalTest4) {
    Integer<int> x(std::string("12345678987654321"));
    Integer<int> y("-12345678987654321");
    ASSERT_LT(y, x);
}

TEST(IntegerFixture, relationalTest5) {
    Integer<int> x(std::string("12345678987654321"));
    Integer<int> y("123456789876");
    ASSERT_GT(x, y);
}

TEST(IntegerFixture, relationalTest6) {
    Integer<int> x(std::string("12345678987654321"));
    Integer<int> y("-12345678987654321");
    ASSERT_GT(x, y);
}

TEST(IntegerFixture, plusDigitsTest1) {
    std::vector<int> a {9, 9, 9};
    std::vector<int> b {1, 1};
    std::vector<int> c;
    std::vector<int> exp {0, 1, 0, 1};
    plus_digits(begin(a), end(a), begin(b), end(b), std::back_inserter(c));
    ASSERT_EQ(c, exp);
}

TEST(IntegerFixture, plusDigitsTest2) {
    std::vector<int> a {9, 9, 9};
    std::vector<int> b;
    std::vector<int> c;
    std::vector<int> exp {0, 1, 0, 1};
    plus_digits(begin(a), end(a), begin(b), end(b), std::back_inserter(c));
    ASSERT_EQ(c, a);
}

TEST(IntegerFixture, subDigitsTest1) {
    std::vector<int> a {9, 9, 9};
    std::vector<int> b {1, 1};
    std::vector<int> c;
    std::vector<int> exp {8, 8, 9};
    minus_digits(begin(a), end(a), begin(b), end(b), std::back_inserter(c));
    ASSERT_EQ(c, exp);
}

TEST(IntegerFixture, subDigitsTest2) {
    std::vector<int> a {9, 9, 9};
    std::vector<int> b;
    std::vector<int> c;
    std::vector<int> exp {0, 1, 0, 1};
    minus_digits(begin(a), end(a), begin(b), end(b), std::back_inserter(c));
    ASSERT_EQ(c, a);
}


TEST(IntegerFixture, leftShiftTest1) {
    std::vector<int> a {9, 9, 9};
    std::vector<int> b;
    std::vector<int> exp {9, 9, 9};
    shift_left_digits(begin(a), end(a), 0, std::back_inserter(b));
    ASSERT_EQ(b, exp);
}

TEST(IntegerFixture, leftShiftTest2) {
    std::vector<int> a {9, 9, 9};
    std::vector<int> b;
    std::vector<int> exp {0, 0, 0, 9, 9, 9};
    shift_left_digits(begin(a), end(a), 3, std::back_inserter(b));
    ASSERT_EQ(b, exp);
}

TEST(IntegerFixture, rightShiftTest1) {
    std::vector<int> a {9, 9, 9};
    std::vector<int> b;
    std::vector<int> exp {9, 9, 9};
    shift_right_digits(begin(a), end(a), 0, std::back_inserter(b));
    ASSERT_EQ(b, exp);
}

TEST(IntegerFixture, rightShiftTest2) {
    std::vector<int> a {9, 9, 9};
    std::vector<int> b;
    std::vector<int> exp {};
    shift_right_digits(begin(a), end(a), 3, std::back_inserter(b));
    ASSERT_EQ(b, exp);
}

TEST(IntegerFixture, rightShiftTest3) {
    std::vector<int> a {1, 2, 3};
    std::vector<int> b;
    std::vector<int> exp {3};
    shift_right_digits(begin(a), end(a), 2, std::back_inserter(b));
    ASSERT_EQ(b, exp);
}

TEST(IntegerFixture, IntegerLeftShiftTest1) {
    Integer<int> a (std::string("1234"));
    ASSERT_EQ(a << 0, a);
}

TEST(IntegerFixture, IntegerLeftShiftTest2) {
    Integer<int> a (std::string("1234"));
    ASSERT_EQ(a << 3, Integer<int>(std::string("1234000")));
}

TEST(IntegerFixture, IntegerLeftShiftTest3) {
    Integer<int> a (std::string("0"));
    ASSERT_EQ(a << 3, Integer<int>(std::string("0")));
}

TEST(IntegerFixture, IntegerRightShiftTest1) {
    Integer<int> a (std::string("1234"));
    ASSERT_EQ(a >> 0, a);
}

TEST(IntegerFixture, IntegerRightShiftTest2) {
    Integer<int> a (std::string("1234"));
    ASSERT_EQ(a >> 3, Integer<int>(std::string("1")));
}

TEST(IntegerFixture, IntegerRightShiftTest3) {
    Integer<int> a (std::string("0"));
    ASSERT_EQ(a >> 3, Integer<int>(std::string("0")));
}

TEST(IntegerFixture, IntegerRightShiftTest4) {
    Integer<int> a (std::string("123"));
    ASSERT_EQ(a >> 5, Integer<int>(std::string("0")));
}