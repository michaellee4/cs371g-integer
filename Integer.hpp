// ---------
// Integer.h
// ---------

#ifndef Integer_h
#define Integer_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <iostream>  // ostream
#include <stdexcept> // invalid_argument
#include <string>    // string
#include <vector>    // vector
#include <algorithm>
#include <map>
#include <iterator>


// -----------------
// shift_left_digits
// -----------------

/**
 * @param b an iterator to the beginning of an input  sequence (inclusive)
 * @param e an iterator to the end       of an input  sequence (exclusive)
 * @param x an iterator to the beginning of an output sequence (inclusive)
 * @return  an iterator to the end       of an output sequence (exclusive)
 * the sequences are of decimal digits
 * output the shift left of the input sequence into the output sequence
 * ([b, e) << n) => x
 */
template <typename II, typename FI>
FI shift_left_digits (II b, II e, int n, FI x) {
    // <your code>
    // Add N trailing 0s
    while (n--) {
        *x ++ = 0;
    }
    // Consume Input Iterator
    while (b != e) {
        *x++ = *b++;
    }
    return x;
}

// ------------------
// shift_right_digits
// ------------------

/**
 * @param b an iterator to the beginning of an input  sequence (inclusive)
 * @param e an iterator to the end       of an input  sequence (exclusive)
 * @param x an iterator to the beginning of an output sequence (inclusive)
 * @return  an iterator to the end       of an output sequence (exclusive)
 * the sequences are of decimal digits
 * output the shift right of the input sequence into the output sequence
 * ([b, e) >> n) => x
 */
template <typename II, typename FI>
FI shift_right_digits (II b, II e, int n, FI x) {
    // <your code>
    int q = n;
    II c = b;
    // Skip the first n elements
    while (q-- && c != e) {
        ++c;
    }
    // Start adding elements since c is N ahead, we will truncate the last N digits
    while (c != e) {
        *x++ = *c++;
    }
    return x;
}

// -----------
// plus_digits
// -----------

/**
 * @param b  an iterator to the beginning of an input  sequence (inclusive)
 * @param e  an iterator to the end       of an input  sequence (exclusive)
 * @param b2 an iterator to the beginning of an input  sequence (inclusive)
 * @param e2 an iterator to the end       of an input  sequence (exclusive)
 * @param x  an iterator to the beginning of an output sequence (inclusive)
 * @return   an iterator to the end       of an output sequence (exclusive)
 * the sequences are of decimal digits
 * output the sum of the two input sequences into the output sequence
 * ([b1, e1) + [b2, e2)) => x
 */
template <typename II1, typename II2, typename FI>
FI plus_digits (II1 b1, II1 e1, II2 b2, II2 e2, FI x) {
    // <your code>
    // Pretty standard grade school implementation of addition
    bool carry = false;
    while (b1 != e1 || b2 != e2 || carry) {
        const typename std::iterator_traits<II1>::value_type d1 = b1 == e1 ? 0 : *b1;
        const typename std::iterator_traits<II2>::value_type d2 = b2 == e2 ? 0 : *b2;
        typename std::iterator_traits<II1>::value_type d3 = d1 + d2 + carry;
        carry = d3 >= 10;
        d3 %= 10;
        *x = d3;
        ++x;
        if (b1 != e1) ++b1;
        if (b2 != e2) ++b2;
    }
    return x;
}

// ------------
// minus_digits
// ------------

/**
 * @param b  an iterator to the beginning of an input  sequence (inclusive)
 * @param e  an iterator to the end       of an input  sequence (exclusive)
 * @param b2 an iterator to the beginning of an input  sequence (inclusive)
 * @param e2 an iterator to the end       of an input  sequence (exclusive)
 * @param x  an iterator to the beginning of an output sequence (inclusive)
 * @return   an iterator to the end       of an output sequence (exclusive)
 * the sequences are of decimal digits
 * output the difference of the two input sequences into the output sequence
 * ([b1, e1) - [b2, e2)) => x
 */
template <typename II1, typename II2, typename FI>
FI minus_digits (II1 b1, II1 e1, II2 b2, II2 e2, FI x) {
    // <your code>
    // Pretty standard grade school implementation of subtraction
    bool borrow = false;
    while (b1 != e1 || b2 != e2) {
        const typename std::iterator_traits<II1>::value_type d1 = b1 == e1 ? 0 : *b1;
        const typename std::iterator_traits<II2>::value_type d2 = b2 == e2 ? 0 : *b2;
        typename std::iterator_traits<II1>::value_type d3 = d1 - d2 - borrow;
        borrow = d3 < 0;
        *x = (10 + (d3) % 10) % 10;
        ++x;
        if (b1 != e1) ++b1;
        if (b2 != e2) ++b2;
    }
    return x;
}

// -----------------
// multiplies_digits
// -----------------

/**
 * @param b  an iterator to the beginning of an input  sequence (inclusive)
 * @param e  an iterator to the end       of an input  sequence (exclusive)
 * @param b2 an iterator to the beginning of an input  sequence (inclusive)
 * @param e2 an iterator to the end       of an input  sequence (exclusive)
 * @param x  an iterator to the beginning of an output sequence (inclusive)
 * @return   an iterator to the end       of an output sequence (exclusive)
 * the sequences are of decimal digits
 * output the product of the two input sequences into the output sequence
 * ([b1, e1) * [b2, e2)) => x
 */
template <typename II1, typename II2, typename FI>
FI multiplies_digits (II1 b1, II1 e1, II2 b2, II2 e2, FI x) {
    // <your code>
    const auto l = std::distance(b1, e1) + std::distance(b2, e2);
    std::vector<typename std::iterator_traits<II1>::value_type> tmp(l, 0);
    for (int i = 0; b1 != e1; ++b1, ++i) {
        if (*b1 == 0) continue;
        int carry = 0;
        for (auto j = 0, bb2 = b2; bb2 != e2 || carry > 0; ++j) {
            int digit = tmp[i + j] + carry + *b1 * (bb2 == e2 ? 0 : *bb2++);
            tmp[i + j] = digit % 10;
            carry = digit / 10;
        }
    }
    std::copy(begin(tmp), end(tmp), x);
    return x;
}


// -------
// Integer
// -------

template <typename T, typename C = std::vector<T>>
class Integer {
    // -----------
    // operator ==
    // -----------

    /**
     * @param lhs   The Integer on the left hand side of the expression
     * @param rhs   The Integer on the left hand side of the expression
     * @return      true if both Integers have the same numeric value and false otherwise
     */
    friend bool operator == (const Integer& lhs, const Integer& rhs) {
        // <your code>
        // Same sign and digits
        return (lhs.negative_ == rhs.negative_ && std::equal(begin(lhs.digits_), end(lhs.digits_), begin(rhs.digits_), end(rhs.digits_)));
    }

    // -----------
    // operator !=
    // -----------

    /**
     * @param lhs   The Integer on the left hand side of the expression
     * @param rhs   The Integer on the left hand side of the expression
     * @return      true if both Integers do not have the same numeric value and false otherwise
     */
    friend bool operator != (const Integer& lhs, const Integer& rhs) {
        return !(lhs == rhs);
    }

    // ----------
    // operator <
    // ----------

    /**
     * @param lhs   The Integer on the left hand side of the expression
     * @param rhs   The Integer on the left hand side of the expression
     * @return      true if lhs is numerically less than rhs
     */
    friend bool operator < (const Integer& lhs, const Integer& rhs) {
        // <your code>
        // If only one is negative then obviously that one is less
        if (lhs.negative_ && !rhs.negative_) return true;
        if (!lhs.negative_ && rhs.negative_) return false;
        assert(lhs.negative_ == rhs.negative_);
        // If one has less digits then it's smaller iff we're comparing positives
        if (lhs.digits_.size() < rhs.digits_.size()) return !lhs.negative_;
        if (lhs.digits_.size() > rhs.digits_.size()) return lhs.negative_;
        // Otherwise, find the number closer to zero. larger digits means smaller number for negative
        return lhs.negative_ ^ std::lexicographical_compare(lhs.digits_.crbegin(), lhs.digits_.crend(), rhs.digits_.crbegin(), rhs.digits_.crend());
    }

    // -----------
    // operator <=
    // -----------

    /**
     * @param lhs   The Integer on the left hand side of the expression
     * @param rhs   The Integer on the left hand side of the expression
     * @return      Same as < except also returns true if lhs == rhs
     */
    friend bool operator <= (const Integer& lhs, const Integer& rhs) {
        return !(rhs < lhs);
    }

    // ----------
    // operator >
    // ----------

    /**
     * @param lhs   The Integer on the left hand side of the expression
     * @param rhs   The Integer on the left hand side of the expression
     * @return      true if lhs is numerically greater than rhs
     */
    friend bool operator > (const Integer& lhs, const Integer& rhs) {
        return (rhs < lhs);
    }

    // -----------
    // operator >=
    // -----------

    /**
     * @param lhs   The Integer on the left hand side of the expression
     * @param rhs   The Integer on the left hand side of the expression
     * @return      Same as > except also returns true if lhs == rhs
     */
    friend bool operator >= (const Integer& lhs, const Integer& rhs) {
        return !(lhs < rhs);
    }

    // ----------
    // operator +
    // ----------

    /**
     * @param lhs   The Integer on the left hand side of the expression
     * @param rhs   The Integer on the left hand side of the expression
     * @return      A copy of the resulting Integer from adding lhs and rhs
     */
    friend Integer operator + (Integer lhs, const Integer& rhs) {
        return lhs += rhs;
    }

    // ----------
    // operator -
    // ----------

    /**
     * @param lhs   The Integer on the left hand side of the expression
     * @param rhs   The Integer on the left hand side of the expression
     * @return      A copy of the resulting Integer from subtracting lhs and rhs
     */
    friend Integer operator - (Integer lhs, const Integer& rhs) {
        return lhs -= rhs;
    }

    // ----------
    // operator *
    // ----------

    /**
     * @param lhs   The Integer on the left hand side of the expression
     * @param rhs   The Integer on the left hand side of the expression
     * @return      A copy of the resulting Integer from multiplying lhs and rhs
     */
    friend Integer operator * (Integer lhs, const Integer& rhs) {
        return lhs *= rhs;
    }

    // -----------
    // operator <<
    // -----------

    /**
     * @param lhs   The Integer on the left hand side of the expression
     * @param rhs   An integer indicating how many digits to shift
     * @return      A copy of the resulting Integer from performing a left digit shift of rhs digits with new digits filled with 0s
     * e.g 1234 << 1 == 12340
     * @throws invalid_argument if (rhs < 0)
     */
    friend Integer operator << (Integer lhs, int rhs) {
        if (rhs < 0) throw std::invalid_argument("Invalid arguments for <<");
        return lhs <<= rhs;
    }

    // -----------
    // operator >>
    // -----------

    /**
     * @param lhs   The Integer on the left hand side of the expression
     * @param rhs   An integer indicating how many digits to shift
     * @return      A copy of the resulting Integer from performing a right digit shift of rhs digits with digits truncated
     * e.g 1234 >> 1 == 123
     * @throws invalid_argument if (rhs < 0)
     */
    friend Integer operator >> (Integer lhs, int rhs) {
        if (rhs < 0) throw std::invalid_argument("Invalid arguments for >>");
        return lhs >>= rhs;
    }

    // -----------
    // operator <<
    // -----------

    /**
     * @param lhs   An ostream to serialize rhs into
     * @param rhs   The Integer to serialize
     * @return      A reference to the ostream after serializing rhs into it
     */
    friend std::ostream& operator << (std::ostream& lhs, const Integer& rhs) {
        // <your code>
        const auto& digits = rhs.digits_;
        // No negative sign for 0
        if (rhs.negative_ && !(digits.size() == 1 && digits.back() == 0)) lhs << '-';
        // Remember we're storing numbers from least to most significant
        for (auto it = digits.rbegin(); it != digits.rend(); ++it) lhs << *it;
        return lhs;
    }

    // ---
    // abs
    // ---

    /**
     * @param x     The integer to take the absolute value of
     * @return      A copy of an integer representing the absolute value of the input Integer
     */
    friend Integer abs (Integer x) {
        return x.abs();
    }

    // ---
    // pow
    // ---

    /**
     * @param x     The integer to take the power of
     * @param e     The power raise the input to
     * @return      A copy of an integer representing the value of x to the power of e
     * @throws invalid_argument if ((x == 0) && (e == 0)) || (e < 0)
     */
    friend Integer pow (Integer x, int e) {
        if ((x == Integer(0) && e == 0) || e < 0) throw std::invalid_argument("Invalid arguments for pow.");
        return x.pow(e);
    }

private:
    // ----
    // data
    // ----

    C digits_; // the backing container
    bool negative_ = false;
    // <your data>

private:
    // -----
    // valid
    // -----

    /**
     * @return Whether the Integer is in a valid state. An Integer is valid if it has any digits
     */
    bool valid () const { // class invariant
        // <your code>
        return !digits_.empty();
    }

    /**
     * A function for post processing any operations on the integer
     * Currently performs two checks
     * 1. Remove any leading 0s
     * 2. Don't allow -0
     */
    void check() {
        // remove leading zeros
        while (digits_.size() > 1 && digits_.back() == 0) digits_.pop_back();
        // No negative 0
        if (digits_.size() == 1 && digits_.front() == 0) negative_ = false;
        if (digits_.empty()) digits_.push_back(0);

        assert(valid());
    }

public:
    // ------------
    // constructors
    // ------------
    Integer() : digits_(1, 0), negative_(false) {};
    /**
     * @param value a long long representing an integral value to set this Integer to
     */
    Integer (long long value) {
        // <your code>
        if (value < 0) {
            value = -value;
            negative_ = true;
        }
        if (value == 0) digits_.push_back(0);
        else {
            while (value > 0) {
                digits_.push_back(value % 10);
                value /=10;
            }
        }

        check();
        assert(valid());
    }

    /**
     * @param value A string representing values too large to fit in a standard numeric type
     * @throws invalid_argument if value is not a valid representation of an Integer
     */
    explicit Integer (const std::string& value) {
        // <your code>
        if (value.empty()) throw std::invalid_argument("Invalid integer string.");
        negative_ = value.front() == '-';
        for (size_t i = ((negative_ || value.front() == '+') ? 1 : 0); i < value.size(); ++i) {
            const char c = value[i];
            if (!('0' <= c && c <= '9')) throw std::invalid_argument("Invalid integer string.");
            digits_.push_back(c - '0');
        }
        std::reverse(begin(digits_), end(digits_));

        check();
        assert(valid());
    }

    Integer             (const Integer&) = default;
    Integer             (Integer&&)      = default;
    ~Integer            ()               = default;
    Integer& operator = (const Integer&) = default;
    Integer& operator = (Integer&&) = default;

    // ----------
    // operator -
    // ----------

    /**
     * Unary negation operator
     * @return A copy of this Integer with the sign negated
     */
    Integer operator - () const {
        // <your code>
        Integer x = *this;
        if (x == Integer(0)) return x;
        x.negative_ = !x.negative_;
        return x;
    } // fix

    // -----------
    // operator ++
    // -----------

    /**
     * pre-increment operator
     * @return A reference to this Integer after pre-incrementing it
     */
    Integer& operator ++ () {
        assert(valid());

        *this += 1;

        check();
        assert(valid());
        return *this;
    }

    /**
     * post-increment operator
     * @return A copy of this Integer before incrementing it
     */
    Integer operator ++ (int) {
        assert(valid());

        Integer x = *this;
        ++(*this);

        check();
        assert(valid());
        return x;
    }

    // -----------
    // operator --
    // -----------

    /**
     * pre-decrement operator
     * @return A reference to this Integer after pre-decrementing it
     */
    Integer& operator -- () {
        assert(valid());

        *this -= 1;

        check();
        assert(valid());
        return *this;
    }

    /**
     * post-decrement operator
     * @return A copy of this Integer before decrementing it
     */
    Integer operator -- (int) {
        assert(valid());

        Integer x = *this;
        --(*this);

        check();
        assert(valid());
        return x;
    }

    bool abs_cmp(const Integer& lhs, const Integer& rhs) {
        if (lhs.digits_.size() < rhs.digits_.size()) return true;
        if (lhs.digits_.size() > rhs.digits_.size()) return false;
        return std::lexicographical_compare(lhs.digits_.crbegin(), lhs.digits_.crend(), rhs.digits_.crbegin(), rhs.digits_.crend());
    }

    // -----------
    // operator +=
    // -----------

    /**
     * add assignment operator
     * @param rhs An Integer representing the amount to add to this integer
     * @return A reference to this Integer after adding rhs to it
     */
    Integer& operator += (const Integer& rhs) {
        // <your code>
        assert(rhs.valid());
        assert(valid());

        C tmp;
        tmp.reserve(2 * digits_.size());
        if (rhs.negative_ == negative_) {
            // your code
            plus_digits(digits_.begin(), digits_.end(), rhs.digits_.begin(), rhs.digits_.end(), std::back_inserter(tmp));
        } else {
            // your code
            // Take the sign of the larger absolute number
            Integer& x = *this;
            const Integer& y = rhs;
            negative_ = (abs_cmp(x, y) ? rhs.negative_ : negative_);
            bool xgy = !abs_cmp(x, y);
            const Integer& larger = xgy ? x : y;
            const Integer& smaller = xgy ? y : x;
            minus_digits(larger.digits_.begin(), larger.digits_.end(), smaller.digits_.begin(), smaller.digits_.end(), std::back_inserter(tmp));
        }
        digits_ = std::move(tmp);

        check();
        assert(valid());
        return *this;
    }

    // -----------
    // operator -=
    // -----------

    /**
     * subtraction assignment operator
     * @param rhs An Integer representing the amount to subtract from this integer
     * @return A reference to this Integer after subtracting rhs to it
     */
    Integer& operator -= (const Integer& rhs) {
        // <your code>
        assert(rhs.valid());
        assert(valid());

        *this += -rhs;

        check();
        assert(valid());
        return *this;
    }

    // -----------
    // operator *=
    // -----------

    // Digit threshold to start using Karatsuba's algorithm. Found experimentally
    static constexpr int kKaratsubaThreshold = 160;

    /**
     * Uses Karatsuba's algorithm to multiply two numbers
     * @param lhs An Integer representing one operand to multiply
     * @param rhs An Integer representing the other operand to multiply
     * @return An Integer representing the result of num1 * num2
     */
    Integer karatsuba(const Integer& num1, const Integer& num2) {
        if (num1.digits_.size() < kKaratsubaThreshold || num2.digits_.size() < kKaratsubaThreshold) return num1 * num2;
        int m = std::max(num1.digits_.size(), num2.digits_.size());
        int m2 = (m + 1) / 2 ;
        Integer xh = num1 >> m2;
        Integer xl = num1 - (xh << m2);
        Integer yh = num2 >> m2;
        Integer yl = num2 - (yh << m2);

        Integer z0 = karatsuba(xl, yl);
        Integer z1 = karatsuba(xl + xh, yl + yh);
        Integer z2 = karatsuba(xh, yh);
        return (z2 << (2 * m2)) + ((z1 - z2 - z0) << m2) + z0;
    }

    /**
     * multiply assignment operator
     * @param rhs An Integer representing the amount to multiply this integer by
     * @return A reference to this Integer after multiplying it by rhs
     */
    Integer& operator *= (const Integer& rhs) {
        // <your code>

        assert(rhs.valid());
        assert(valid());

        C tmp;
        tmp.reserve(2 * digits_.size());
        if (digits_.size() > kKaratsubaThreshold && rhs.digits_.size() > kKaratsubaThreshold) {
            tmp = std::move(karatsuba(*this, rhs).digits_);
        } else {
            multiplies_digits(begin(digits_), end(digits_), begin(rhs.digits_), end(rhs.digits_), std::back_inserter(tmp));
        }
        digits_ = std::move(tmp);
        negative_ = (negative_ != rhs.negative_);

        check();
        assert(valid());
        return *this;
    }

    // ------------
    // operator <<=
    // ------------

    /**
     * left digit shift assignment operator
     * @param n A numeric representing the number of digits to shift by
     * @return A reference to this Integer after shifting it to the left by n digits
     */
    Integer& operator <<= (int n) {
        // <your code>
        assert(n >= 0);
        assert(valid());

        C tmp(digits_.size() + n, 0);
        std::copy(begin(digits_), end(digits_), begin(tmp) + n);
        digits_ = std::move(tmp);

        check();
        assert(valid());
        return *this;
    }

    // ------------
    // operator >>=
    // ------------

    /**
     * right digit shift assignment operator
     * @param n A numeric representing the number of digits to shift by
     * @return A reference to this Integer after shifting it to the right by n digits
     */
    Integer& operator >>= (int n) {
        // <your code>
        assert(n >= 0);
        assert(valid());

        C tmp(std::max((int) digits_.size() - n, 1));
        if ((int) digits_.size() > n) {
            std::copy(begin(digits_) + n, end(digits_), begin(tmp));
        }
        digits_ = std::move(tmp);

        check();
        assert(valid());
        return *this;
    }

    // ---
    // abs
    // ---

    /**
     * absolute value
     * @return A reference to this Integer after taking its absolute value
     */
    Integer& abs () {
        // <your code>
        assert(valid());

        negative_ = false;

        check();
        assert(valid());
        return *this;
    }

    // ---
    // pow
    // ---

    /**
     * power
     * @param e A numeric representing what power to take
     * @return A reference to this Integer taking it to the e'th power
     * @throws invalid_argument if ((this == 0) && (e == 0)) or (e < 0)
     * Utilizes squared exponentiation for O(logE) * O(mul) runtime
     */
    Integer& pow (int e) {
        // <your code>
        assert(valid());
        if ((*this == Integer(0) && e == 0) || e < 0) throw std::invalid_argument("Invalid arguments for pow.");
        if (*this == Integer(0) || *this == (Integer(1))) return *this;

        Integer x = *this;
        --e;
        while(e > 0) {
            if (e % 2 == 1) {
                *this *= x;
            }
            e >>= 1;
            // Adding this if check was actually the difference between
            // receiving TLE and not. I had to comb through Java BigInt
            // source code to find this optimization. Though, I guess it
            // makes sense why this works, since the size of our integers roughly
            // double every time we square them the final multiplication will
            // always dominate the prior ones.
            if (e > 0) {
                x *= x;
            }
        }

        check();
        assert(valid());
        return *this;
    }
};

#endif // Integer_h
