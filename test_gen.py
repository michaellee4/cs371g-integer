import operator
import random

ops = {
    '+' : operator.add,
    '-' : operator.sub,
    '*' : operator.mul,
    '**': lambda x, y : x ** y
}
keys = list(ops.keys())
def ndigitint(n):
    return int(''.join([str(random.randint(0, 9)) for _ in range(n)]))
with open('RunInteger.in', 'w+') as inputf, open('RunInteger.out', 'w+') as outputf:
    for i in range(250):
        x, y = ndigitint(random.randint(22, 30)), ndigitint(random.randint(22, 30))
        x *= random.choice([-1, 1])
        y *= random.choice([-1, 1])
        op = random.choice(keys)
        if op == '**':
            y = random.randint(0, 100)
        inputf.write(f"{x} {op} {y}\n")
        # outputf.write(f"{x} {op} {y} = {ops[op](x, y)}\n")
        outputf.write(f"{ops[op](x, y)}\n")
