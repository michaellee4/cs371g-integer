# CS371g: Generic Programming Integer Repo

* Name: Michael Lee

* EID: ml45898

* GitLab ID: michaellee4

* HackerRank ID: michaellee4

* Git SHA: d92b0feccf75e457b97fb8e71a88892510125bd9 

* GitLab Pipelines: https://gitlab.com/michaellee4/cs371g-integer/-/pipelines

* Estimated completion time: 10 hours

* Actual completion time: 20 hours
    - ~ 5 hours to finish
    - ~ 15 hours to debug and optimize

* Comments:
    - I've implemented both Karatsuba's multiplication algorithm and exponentiation by squaring
    - The difference between passing the final Hackerrank test and not passing it was literally one if statement in my power function
        - I want to give a shoutout to Java BigInt source code for helping me find that.
        - I gave an explanation as to what happened in a comment in my pow function 
        - Overall I still feel that the bound on the final test case was way to tight
    - I know that my Karatsuba's algorithm is at least more efficient than the naive implementation since I was able to multiply 2 ~30,000 digit numbers in ~0.7 seconds vs ~2.4 seconds for the naive implementation

