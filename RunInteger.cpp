// --------------
// RunInteger.c++
// --------------

// --------
// includes
// --------

#include <iostream> // cin, cout, istream, ostream
#include <string>
#include <sstream>
#include "Integer.hpp"

using namespace std;

// ------------
// integer_read
// ------------

// ------------
// integer_eval
// ------------

// -------------
// integer_print
// -------------

// ----
// main
// ----

int main () {
    // <your code>
    std::ios::sync_with_stdio(false);
    std::string buf, i, j, op;
    while (std::getline(cin, buf)) {
        std::stringstream s(buf);
        s >> i >> op >> j;
        Integer<int> x(i);
        Integer<int> y(j);
        Integer<int> z(0);
        if (op == "+") {
            z = x + y;
        } else if (op == "-") {
            z = x - y;
        } else if (op == "*") {
            z = x * y;
        } else if (op == "**") {
            z = x.pow(std::stoi(j));
        }
        // Nicer looking string for easier debugging
        // std::cout << buf << " = " << z << std::endl;
        std::cout << z << std::endl;
    }
    return 0;
}
